# HTTP, REST, WebSockets homework

## Demo

[View demo](https://websockets-typing-game.herokuapp.com)

## Instalation

**Frontend:**

`cd client`

`npm install`

`npm run build-dev`

**Backend:**

`cd server`

`npm install`

`npm start` or `npm run start-watch`

**Open** <http://localhost:3002/>
