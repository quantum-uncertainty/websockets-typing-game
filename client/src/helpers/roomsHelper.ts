import {
  roomsContainer,
  roomsPageEl,
  gamePageEl,
  createRoomButton,
  leaveRoomButton,
} from './viewHelper';

import {
  createElement,
  createElementWithText,
  hideElement,
  showElement,
  clearElement,
} from './domHelper';

type Room = { id: string, users: string[] };

export default function roomsHelper(socket: SocketIOClient.Socket): void {
  socket.on('UPDATE_ROOMS', onUpdateRooms);
  socket.on('JOIN_ROOM_DONE', onJoinRoomDone);
  createRoomButton.addEventListener('click', createRoom);
  leaveRoomButton.addEventListener('click', leaveRoom);

  function onUpdateRooms(rooms: Room[]) {
    const roomElements = rooms.map(createRoomElement);
    clearElement(roomsContainer);
    roomsContainer.append(...roomElements);
  }

  function onJoinRoomDone(roomId: string) {
    sessionStorage.setItem('currentRoom', roomId);
    hideElement(roomsPageEl);
    showElement(gamePageEl);
    const titleEl = document.getElementById('game-title') as HTMLElement;
    titleEl.innerText = roomId;
  }

  function createRoom() {
  // eslint-disable-next-line no-alert
    const name = prompt('Enter room name:')?.trim();
    if (name !== undefined) {
      if (name.length > 0) {
        socket.emit('CREATE_ROOM', name);
      } else {
        // eslint-disable-next-line no-alert
        alert('Name can not be empty!');
      }
    }
  }

  function leaveRoom() {
    socket.emit('LEAVE_ROOM', sessionStorage.getItem('currentRoom'));
    hideElement(gamePageEl);
    showElement(roomsPageEl);
  }

  function createRoomElement({ id, users }: Room) {
    const connectedUsersEl = createElementWithText('p', `${users.length} users connected`);
    const headerEl = createElementWithText('h2', id);

    const joinButtonEl = createElementWithText('button', 'Join', { className: 'button' });
    joinButtonEl.addEventListener('click', () => {
      socket.emit('JOIN_ROOM', id);
    });

    const roomElement = createElement('div', { className: 'room-item' });
    roomElement.append(connectedUsersEl, headerEl, joinButtonEl);
    return roomElement;
  }
}
