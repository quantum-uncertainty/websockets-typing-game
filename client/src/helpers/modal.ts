import { createElement } from './domHelper';

interface ModalParameters {
  title: string;
  bodyElement: HTMLElement;
  onClose?: () => void;
}

export default function showModal(modalParams: ModalParameters): void {
  const root = getModalContainer() as HTMLElement;
  const modal = createModal(modalParams);
  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }: ModalParameters) {
  const layer = createElement('div', { className: 'modal-layer' });
  const modalContainer = createElement('div', { className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose?: () => void) {
  const headerElement = createElement('div', { className: 'modal-header' });
  const titleElement = createElement('span');
  const closeButton = createElement('div', { className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    if (onClose) onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(title, closeButton);

  return headerElement;
}

function hideModal() {
  const modal = document.querySelector('.modal-layer') as HTMLElement;
  modal.remove();
}
