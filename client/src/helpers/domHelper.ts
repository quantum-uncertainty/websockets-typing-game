interface CreateElementOptions {
  className?: string;
  attributes?: { [key: string]: string | number };
}

export function createElement(tagName: string,
  options: CreateElementOptions = {}): HTMLElement {
  const { className, attributes } = options;
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (attributes) {
    Object.keys(attributes).forEach((key) => (
      element.setAttribute(key, attributes[key] as string)
    ));
  }
  return element;
}

export function createElementWithText(
  tagName: string,
  text = '',
  options?: CreateElementOptions,
): HTMLElement {
  const element = createElement(tagName, options);
  element.innerText = text;
  return element;
}

export function hideElement(element: HTMLElement): void {
  element.classList.add('display-none');
}

export function showElement(element: HTMLElement): void {
  element.classList.remove('display-none');
}

export function clearElement(element: HTMLElement): void {
  element.innerHTML = '';
}
