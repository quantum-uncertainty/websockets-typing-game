export const roomsPageEl = document.querySelector('.rooms-page') as HTMLElement;
export const gamePageEl = document.querySelector('.game-page') as HTMLElement;
export const createRoomButton = document.querySelector('.create-room-button') as HTMLButtonElement;
export const roomsContainer = document.querySelector('.rooms-container') as HTMLElement;
export const leaveRoomButton = document.querySelector('.leave-room-button') as HTMLButtonElement;
export const usersContainer = document.querySelector('.users-container') as HTMLElement;
export const readyButton = document.querySelector('.button.ready') as HTMLElement;

export const gameRoomEl = document.querySelector('.game-room') as HTMLElement;
export const timerEl = document.querySelector('.timer') as HTMLElement;
export const gameTextEl = document.querySelector('.game-text') as HTMLElement;
export const correctTextEl = document.querySelector('.game-text .correct') as HTMLElement;
export const wrongTextEl = document.querySelector('.game-text .wrong') as HTMLElement;
export const nextTextEl = document.querySelector('.game-text .next') as HTMLElement;
export const remainingTextEl = document.querySelector('.game-text .remaining') as HTMLElement;
