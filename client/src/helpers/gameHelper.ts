import {
  readyButton,
  leaveRoomButton,
  usersContainer,
  gameTextEl,
  correctTextEl,
  wrongTextEl,
  nextTextEl,
  remainingTextEl,
  timerEl,
  gameRoomEl,
} from './viewHelper';

import {
  createElement,
  clearElement,
  hideElement,
  showElement,
  createElementWithText,
} from './domHelper';

import showModal from './modal';

type User = {
  username: string;
  isReady: boolean;
}

export default function gameHelper(socket: SocketIOClient.Socket): void {
  const game = {
    username: sessionStorage.getItem('username') as string,
    userReady: false,
    hasEnded: false,
    text: '',
    input: '',
  };

  socket.on('UPDATE_USERS', updateUsers);
  socket.on('START_GAME', initGame);
  socket.on('BROADCAST_PROGRESS', updateProgress);
  socket.on('END_GAME', endGame);

  function updateUsers(users: User[]) {
    const userElements = users.map(createUserElement);
    clearElement(usersContainer);
    usersContainer.append(...userElements);
  }

  function updateProgress(username: string, progress: number) {
    const progressBar = document.querySelector(`.progress-bar.${username}`) as HTMLElement;
    progressBar.style.width = `${progress}%`;
    if (progress === 100) {
      progressBar.classList.add('done');
    }
  }

  type Player = {
    username: string;
    progress: number
    finishTime: number;
  };

  function endGame(results: Player[]) {
    game.hasEnded = true;
    game.input = '';
    game.text = '';
    document.removeEventListener('keydown', onInput);
    const playerElements = results.map((player) => (createElementWithText('li',
      `${player.username} (${Math.round(player.progress)}% in ${player.finishTime} seconds)`)
    ));
    const listElement = createElement('ol');
    listElement.append(...playerElements);
    const bodyElement = createElement('div', { className: 'modal-body' });
    bodyElement.append(listElement);
    const onClose = () => {
      hideElement(gameTextEl);
      readyButton.click();
      showElement(readyButton);
      showElement(leaveRoomButton);
    };
    showModal({ title: 'Game over!', bodyElement, onClose });
  }

  const SECOND = 1000;

  function initGame(secondsBeforeStart: number, secondsForGame: number, textId: string) {
    game.hasEnded = false;
    hideElement(readyButton);
    hideElement(leaveRoomButton);

    let timeLeft = secondsBeforeStart;
    timerEl.innerHTML = `${timeLeft}`;
    showElement(timerEl);

    const timerId = setInterval(() => {
      timeLeft--;
      timerEl.innerHTML = `${timeLeft}`;
      if (timeLeft === 0) {
        hideElement(timerEl);
        clearInterval(timerId);
        getText(textId)
          .then(startGame);
        startGameTimer(secondsForGame);
      }
    }, SECOND);
  }

  async function getText(textId: string) {
    const response = await fetch(`/game/texts/${textId}`);
    return response.text();
  }

  function startGameTimer(secondsForGame: number) {
    const timeLeftEl = createElement('span', { className: 'time-left' });
    gameRoomEl.appendChild(timeLeftEl);
    let timeLeft = secondsForGame;
    const timerId = setInterval(() => {
      timeLeft--;
      timeLeftEl.innerText = `${timeLeft} seconds left`;
      if (timeLeft === 0 || game.hasEnded) {
        clearInterval(timerId);
        timeLeftEl.remove();
      }
    }, SECOND);
  }

  function startGame(text: string) {
    game.text = text;
    showElement(gameTextEl);
    clearElement(correctTextEl);
    clearElement(wrongTextEl);
    nextTextEl.innerText = text.charAt(0);
    remainingTextEl.innerText = text.slice(1);

    document.addEventListener('keydown', onInput);
  }

  function onInput(event: KeyboardEvent) {
    const { text } = game;
    if (event.key === 'Backspace') {
      game.input = game.input.slice(0, -1);
    } else if (/^[ -~\n]$/.test(event.key)) {
      game.input += event.key;
    }
    const inputLength = game.input.length;

    let correctInput = '';
    for (let i = inputLength; i > 0; i--) {
      const inputSubstr = game.input.substring(0, i);
      if (text.startsWith(inputSubstr)) {
        correctInput = inputSubstr;
        break;
      }
    }
    correctTextEl.innerText = correctInput;

    const wrongInput = text.substring(correctInput.length, inputLength);
    wrongTextEl.innerText = wrongInput;

    const nextChar = text.charAt(inputLength);
    nextTextEl.innerText = nextChar;

    const remainingText = text.substring(inputLength + 1);
    remainingTextEl.innerText = remainingText;

    const progress = (correctInput.length / text.length) * 100;
    socket.emit('UPDATE_PROGRESS', sessionStorage.getItem('currentRoom'), progress);
  }

  function createUserElement({ username, isReady }: User) {
    const usernameEl = createElement('p', { className: 'user' });
    if (username === game.username) {
      usernameEl.classList.add('you');
    }

    const readyIndicator = createElement('span', { className: 'indicator' });
    readyIndicator.classList.add(isReady ? 'ready' : 'not-ready');

    usernameEl.append(readyIndicator, username);

    const progressBarWrapper = createElement('div', { className: 'progress-bar-wrapper' });
    const progressBar = createElement('div', { className: `progress-bar ${username}` });
    progressBarWrapper.append(progressBar);

    const userElement = createElement('div');
    userElement.append(usernameEl, progressBarWrapper);
    return userElement;
  }

  readyButton.addEventListener('click', () => {
    game.userReady = !game.userReady;
    socket.emit('UPDATE_READY_STATUS', game.userReady, game.username);
    if (game.userReady) {
      readyButton.innerText = 'Not ready';
    } else {
      readyButton.innerText = 'Ready';
    }
  });
}
