import io from 'socket.io-client';
import roomsHelper from './helpers/roomsHelper';
import gameHelper from './helpers/gameHelper';

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username } });

roomsHelper(socket);
gameHelper(socket);

socket.on('CHECK_USERNAME', (isAvailable: boolean) => {
  if (!isAvailable) {
    // eslint-disable-next-line no-alert
    alert(`Username '${username}' is not available. Please try again`);
    sessionStorage.removeItem('username');
    window.location.replace('/login');
  }
});

// eslint-disable-next-line no-alert
socket.on('ERROR', (message: string) => alert(message));
