(function checkAlreadyLoggedIn() {
  const username = sessionStorage.getItem('username');

  if (username) {
    window.location.replace('/game');
  }
}());

const submitButton = document.getElementById('submit-button') as HTMLButtonElement;
const input = document.getElementById('username-input') as HTMLInputElement;

const getInputValue = () => input.value.trim();

const onClickSubmitButton = async () => {
  const username = getInputValue();
  if (!username) {
    return;
  }
  sessionStorage.setItem('username', username);
  window.location.replace('/game');
};

const onKeyUp = (ev: KeyboardEvent) => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

submitButton.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);
