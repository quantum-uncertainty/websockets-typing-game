const path = require('path');

module.exports = (env = { NODE_ENV: 'development' }) => {
  const mode = env.NODE_ENV;
  const isDevMode = mode === 'development';

  return {
    mode,
    watch: isDevMode,
    entry: {
      login: './src/login.ts',
      game: './src/game.ts',
    },
    output: {
      filename: '[name].js',
      path: path.join(__dirname, 'dist'),
    },
    module: {
      rules: [
        {
          test: /\.js|ts$/,
          exclude: /node_modules/,
          resolve: {
            extensions: ['.ts', '.js']
          },
          use: [
            {
              loader: 'babel-loader',
              options: {
                configFile: './babel.config.json',
                cacheDirectory: true,
              },
            },
            'ts-loader',
          ],
        },
      ],
    },
    devServer: {
      contentBase: path.join(__dirname, 'public'),
    },
    devtool: isDevMode ? 'inline-source-map' : 'source-map',
  };
};
