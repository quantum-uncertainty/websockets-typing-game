import roomService from '../../services/roomService';

interface RoomsHelper {
  createRoom: (roomId: string) => void;
  joinRoom: (roomId: string) => void;
  leaveRoom: (roomId: string) => void;
  disconnect: () => void;
}

export default function roomsHelper(
  io: SocketIO.Server,
  socket: SocketIO.Socket,
  username:string,
): RoomsHelper {
  return {
    createRoom,
    joinRoom,
    leaveRoom,
    disconnect,
  };

  function createRoom(roomId: string) {
    if (roomService.roomExists(roomId)) {
      socket.emit('ERROR', `Room ${roomId} already exists`);
      return;
    }
    socket.join(roomId, () => {
      roomService.createRoom(roomId, username);
      socket.emit('JOIN_ROOM_DONE', roomId);
      socket.emit('UPDATE_USERS', roomService.getRoomUsers(roomId));
      broadcastUpdatedRooms();
    });
  }

  function joinRoom(roomId: string) {
    const canJoinRoom = roomService.getById(roomId)?.isAvailable;
    if (!canJoinRoom) {
      socket.emit('ERROR', 'Room is unavailable');
      return;
    }
    socket.join(roomId);
    roomService.addUserToRoom(roomId, username);
    socket.emit('JOIN_ROOM_DONE', roomId);
    sendUpdatedUsersToRoom(roomId);
    broadcastUpdatedRooms();
  }

  function leaveRoom(roomId: string) {
    socket.leave(roomId);
    roomService.removeUserFromRoom(roomId, username);
    sendUpdatedUsersToRoom(roomId);
    broadcastUpdatedRooms();
  }

  function disconnect(): void {
    const room = roomService.getUserRoom(username);
    if (room) {
      roomService.removeUserFromRoom(room.id, username);
      sendUpdatedUsersToRoom(room.id);
      broadcastUpdatedRooms();
    }
  }

  function broadcastUpdatedRooms() {
    io.emit('UPDATE_ROOMS', roomService.availableRooms);
  }

  function sendUpdatedUsersToRoom(roomId: string) {
    io.in(roomId).emit('UPDATE_USERS', roomService.getRoomUsers(roomId));
  }
}
