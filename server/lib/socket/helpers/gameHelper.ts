import roomService from '../../services/roomService';
import gameService from '../../services/gameService';
import type Player from '../../models/game';
import { texts } from '../../data';
import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from '../../gameConfig';

interface GameHelper {
  updateReadyStatus: (isReady: boolean, username: string) => void;
  tryStartGame: (roomId: string) => void;
  updateProgress: (roomId: string, username: string, progress: number) => void;
  endGame: (roomId: string, results: Player[]) => void;
}

export default function gameHelper(io: SocketIO.Server): GameHelper {
  return {
    updateReadyStatus,
    tryStartGame,
    updateProgress,
    endGame,
  };

  function updateReadyStatus(isReady: boolean, username: string) {
    const room = roomService.getUserRoom(username);
    if (room) {
      room.updateUser(username, { isReady });
      io.in(room.id).emit('UPDATE_USERS', room.users);
      tryStartGame(room.id);
    }
  }

  function tryStartGame(roomId: string) {
    const room = roomService.getById(roomId);
    if (room?.everyoneReady) {
      if (gameService.isGameActive(roomId)) {
        return;
      }
      const textId = Math.round(Math.random() * (texts.length - 1));
      io.in(room.id).emit(
        'START_GAME',
        SECONDS_TIMER_BEFORE_START_GAME,
        SECONDS_FOR_GAME,
        textId,
      );
      gameService.startGame(room);
      io.emit('UPDATE_ROOMS', roomService.availableRooms);
    }
  }

  function updateProgress(roomId: string, username: string, progress: number) {
    gameService.updateProgress(roomId, username, progress);
    io.in(roomId).emit('BROADCAST_PROGRESS', username, progress);
  }

  function endGame(roomId: string, results: Player[]) {
    if (gameService.isGameActive(roomId)) {
      gameService.endGame(roomId);
      io.in(roomId).emit('END_GAME', results);
      io.in(roomId).emit('UPDATE_USERS', roomService.getById(roomId)?.users);
    }
  }
}
