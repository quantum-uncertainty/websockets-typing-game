import roomService from '../services/roomService';
import roomsHelper from './helpers/roomsHelper';
import gameHelper from './helpers/gameHelper';
import gameService from '../services/gameService';

const usernames: Set<string> = new Set();

export default (io: SocketIO.Server): void => {
  const {
    updateReadyStatus,
    tryStartGame,
    updateProgress,
    endGame,
  } = gameHelper(io);

  gameService.emitter.on('game_ended', endGame);

  io.on('connection', (socket) => {
    const username = socket.handshake.query.username.trim();
    const isAvailableUsername = !usernames.has(username);
    socket.emit('CHECK_USERNAME', isAvailableUsername);
    if (isAvailableUsername) {
      usernames.add(username);
      socket.emit('UPDATE_ROOMS', roomService.availableRooms);
    }

    let currentRoomId = '';

    const {
      createRoom,
      joinRoom,
      leaveRoom,
      disconnect,
    } = roomsHelper(io, socket, username);

    socket.on('CREATE_ROOM', (roomId) => {
      createRoom(roomId);
      currentRoomId = roomId;
    });

    socket.on('JOIN_ROOM', (roomId) => {
      joinRoom(roomId);
      currentRoomId = roomId;
      tryStartGame(roomId);
    });

    socket.on('LEAVE_ROOM', (roomId) => {
      leaveRoom(roomId);
      currentRoomId = '';
      tryStartGame(roomId);
      gameService.removePlayerFromGame(currentRoomId, username);
    });

    socket.on('UPDATE_READY_STATUS', updateReadyStatus);

    socket.on('UPDATE_PROGRESS', (roomId: string, progress: number) => {
      updateProgress(roomId, username, progress);
    });

    socket.on('disconnect', () => {
      disconnect();
      if (currentRoomId) {
        tryStartGame(currentRoomId);
        gameService.removePlayerFromGame(currentRoomId, username);
      }
      usernames.delete(username);
    });
  });
};
