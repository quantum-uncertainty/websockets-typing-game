import express from 'express';
import http from 'http';
import socketIO from 'socket.io';
import socketHandler from './socket';
import routes from './routes';
import { STATIC_PATH, SCRIPTS_PATH, PORT } from './config';

const app = express();
const httpServer = new http.Server(app);
const io = socketIO(httpServer);

app.use(express.static(STATIC_PATH));

app.use('/scripts', express.static(SCRIPTS_PATH));

routes(app);

app.get('*', (_req, res) => {
  res.redirect('/login');
});

socketHandler(io);

httpServer.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is listening on port ${PORT}`);
});
