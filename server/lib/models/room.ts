import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../gameConfig';

type User = {
  username: string;
  isReady: boolean;
};

type UpdateUserData = {
  isReady?: boolean;
};

export default class Room {
  id: string;

  users: User[];

  gameInProgress: boolean;

  constructor(id: string) {
    this.id = id;
    this.users = [];
    this.gameInProgress = false;
  }

  get usersCount(): number {
    return this.users.length;
  }

  get isFull(): boolean {
    return this.usersCount === MAXIMUM_USERS_FOR_ONE_ROOM;
  }

  get isAvailable(): boolean {
    return !this.isFull && !this.gameInProgress;
  }

  get everyoneReady(): boolean {
    return this.users.every((user) => user.isReady);
  }

  reset(): void {
    this.users.forEach((user) => {
      user.isReady = false;
    });
    this.gameInProgress = false;
  }

  hasUser(username: string): boolean {
    return this.users.some((user) => user.username === username);
  }

  getUser(username: string): User | undefined {
    return this.users.find((user) => user.username === username);
  }

  addUser(username: string): void {
    if (this.hasUser(username)) {
      return;
    }
    const newUser = { username, isReady: false };
    this.users.push(newUser);
  }

  removeUser(username: string): void {
    if (this.hasUser(username)) {
      this.users = this.users.filter((user) => user.username !== username);
    }
  }

  updateUser(username: string, updateData: UpdateUserData): void {
    const user = this.getUser(username);
    if (user) {
      Object.assign(user, updateData);
    }
  }
}
