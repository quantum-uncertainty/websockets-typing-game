import type { EventEmitter } from 'events';
import Room from './room';
import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from '../gameConfig';

export type Player = {
  username: string;
  progress: number
  finishTime: number;
};

export default class Game {
  room: Room;

  players: Player[];

  emitter: EventEmitter;

  secondsElapsed: number;

  constructor(room: Room, emitter: EventEmitter) {
    this.room = room;
    this.players = room.users.map(({ username }) => ({
      username,
      progress: 0,
      finishTime: SECONDS_FOR_GAME,
    }));
    this.emitter = emitter;
    this.secondsElapsed = 0;

    const SECOND = 1000;

    setTimeout(() => {
      const intervalId = setInterval(() => {
        this.secondsElapsed++;
        if (this.hasEnded) {
          clearInterval(intervalId);
          this.emitter.emit('game_ended', this.room.id, this.results);
        }
      }, SECOND);
    }, SECOND * SECONDS_TIMER_BEFORE_START_GAME);
  }

  get hasEnded(): boolean {
    return this.secondsElapsed === SECONDS_FOR_GAME
     || this.players.every((p) => p.progress === 100);
  }

  get results(): Player[] {
    return this.players.sort((p1, p2) => (
      p1.finishTime - p2.finishTime || p2.progress - p1.progress
    ));
  }

  updateProgress(username: string, progress: number): void {
    if (progress > 100) return;
    const player = this.players.find((p) => p.username === username);
    if (player) {
      player.progress = progress;
      if (progress === 100) {
        player.finishTime = this.secondsElapsed;
      }
    }
  }

  removePlayer(username: string): void {
    this.players = this.players.filter((p) => p.username !== username);
  }
}
