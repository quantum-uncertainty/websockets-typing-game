import { Application } from 'express';
import loginRoutes from './loginRoutes';
import gameRoutes from './gameRoutes';

export default (app: Application): void => {
  app.use('/login', loginRoutes);
  app.use('/game', gameRoutes);
};
