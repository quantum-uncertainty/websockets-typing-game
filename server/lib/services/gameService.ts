import { EventEmitter } from 'events';
import Game from '../models/game';
import Room from '../models/room';

class GameService {
  private games: Map<string, Game>;

  emitter: EventEmitter;

  constructor() {
    this.games = new Map();
    this.emitter = new EventEmitter();
  }

  startGame(room: Room) {
    if (this.isGameActive(room.id)) {
      return;
    }
    room.gameInProgress = true;
    const game = new Game(room, this.emitter);
    this.games.set(room.id, game);
  }

  isGameActive(roomId: string): boolean {
    return this.games.has(roomId);
  }

  updateProgress(roomId: string, username: string, progress: number) {
    const game = this.games.get(roomId);
    if (game) {
      game.updateProgress(username, progress);
    }
  }

  removePlayerFromGame(roomId: string, username: string) {
    const game = this.games.get(roomId);
    if (game) {
      game.removePlayer(username);
    }
  }

  endGame(roomId: string) {
    const game = this.games.get(roomId);
    if (game) {
      game.room.reset();
      this.games.delete(roomId);
    }
  }
}

const gameService = new GameService();
export default gameService;
