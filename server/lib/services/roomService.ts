import Room from '../models/room';

class RoomService {
  private rooms: Room[];

  constructor() {
    this.rooms = [];
  }

  get availableRooms() {
    return this.rooms.filter((room) => room.isAvailable);
  }

  getById(roomId: string): Room | undefined {
    return this.rooms.find((room) => room.id === roomId);
  }

  roomExists(roomId: string): boolean {
    return Boolean(this.getById(roomId));
  }

  createRoom(roomId: string, user: string): void {
    const newRoom = new Room(roomId);
    newRoom.addUser(user);
    this.rooms.push(newRoom);
  }

  removeRoom(roomId: string): void {
    this.rooms = this.rooms.filter((room) => room.id !== roomId);
  }

  addUserToRoom(roomId: string, user: string): void {
    const room = this.getById(roomId);
    if (room) {
      room.addUser(user);
    }
  }

  removeUserFromRoom(roomId: string, user: string): void {
    const room = this.getById(roomId);
    if (room) {
      room.removeUser(user);
      if (room.usersCount === 0) {
        this.removeRoom(roomId);
      }
    }
  }

  getUserRoom(user: string): Room | undefined {
    return this.rooms.find((room) => room.hasUser(user));
  }

  getRoomUsers(roomId: string) {
    return this.getById(roomId)?.users;
  }
}

const roomService = new RoomService();
export default roomService;
